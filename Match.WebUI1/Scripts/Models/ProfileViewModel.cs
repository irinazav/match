﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Match.Domain.Entities;

namespace Match.WebUI.Models
{
    public class Query
    {
        public Profile Profile { get; set; }  

        public IEnumerable<Status> Statuses {get; set;}  
        public IEnumerable<Religion> Religions {get; set;}    
        public IEnumerable<Country> Countries {get; set;}  
        public IEnumerable<State> States {get; set;}  
        public IEnumerable<Height> Heightes {get; set;}  
        public IEnumerable<Weight> Weightes {get; set;}
        public IEnumerable<Smoke> Smokes { get; set; }  
        public IEnumerable<SearchAge> SearchAges {get; set;}  
        public IEnumerable<Children> Childrens {get; set;} 
        public IEnumerable<Disability> Disabilities {get; set;}  
        public IEnumerable<EducationGrade> EducationGrades {get; set;}  
            
            
            
    }
}