﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations.Schema;

namespace Match.Domain.Entities
{
    public class Religion : EntityBase<int>, IAttribute
    {
        public string Name { get; set; }

        [InverseProperty("ReligionId")]
        public virtual IEnumerable<Profile> Profiles { get; set; }

       

        public static string[] InitValue = {"Atheism", "Baha'i", "Buddhism", "Christianity", "Confucianism", "Hinduism", 
        "Islam", "Jainism", "Judaism", "Shinto", "Sikhism", "Taoism", "Zoroastrianism"};
    }
}
