﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Match.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;



namespace Match.Domain.Entities
{

    public static class Numerology1 
    {
      

      
        // http://astrology-numerology.com/num-relationship.html

        public int[,] InitValue = new int[,] {
        {1, 1}, {1, 5}, {1, 7},
        {2, 2}, {2, 4}, {2, 8},
        {3, 3}, {3, 6}, {3, 9},
        {4, 2}, {4, 4}, {4, 8},
        {5, 1}, {5, 5}, {5, 7},
        {6, 3}, {6, 6}, {6, 9},
        {7, 1}, {7, 5}, {7, 7},
        {8, 2}, {8, 4}, {8, 8},
        {9, 3}, {9, 6}, {9, 9}      
        };
       
   
        

        public static int GetLifePathNumber(DateTime date)
        {
            //Cycle Method           
            int lifePathNumber = GetNumber(date.Month.ToString()
                      .ToCharArray().Sum(p => p)) +
                         GetNumber(date.Year.ToString().ToCharArray().Sum(p => p)) +
                         GetNumber(date.Day.ToString().ToCharArray().Sum(p => p));
            lifePathNumber = GetNumber(lifePathNumber);

           return lifePathNumber;
                    
        }
      
    

    private static int GetNumber(int n)
    {
         do
            {
                n = n.ToString().
                       ToCharArray().AsEnumerable()
                          .Sum(nn => Int32.Parse(nn.ToString()));
            } while (n > 9);
            return n;     
    }


   
}




/*Numerology based love compatibility test is based on the following numbers - Life path number, destiny number, birthday number and balance number. In fact, every input is converted to some number and finally compatibility between these two numbers is determined. 
LifePathNumber
Date of Birth – December, 30, 1989 = 12 / 30 / 1989
3 Cycle Method
[i] (1 + 2) + (3 + 0) + (1 + 9 + 8 + 9) = 3 + 3 + 27 = 3 + 3 + 9 = 15 = 6 (your 3 Cycles are 3, 3, and 9)
Personality Number is one of the five core numbers that include your Life Path Number, Heart's Desire (aka Soul Urge) , Day Number (the day you were born on), and Expression (aka Destiny) Number. 

1 - 3,5
2 - 8,9
3- 5,7
4 - 8
5 -*/