﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match.Domain.Abstract;

namespace Match.Repository.Abstract
{
    public interface IRepositoryCRUD<T, TId> : IRepositoryCUD<T, TId>, IRepositoryR<T, TId>
                                                     where T : class, IAggregateRoot
    {
    }
}
