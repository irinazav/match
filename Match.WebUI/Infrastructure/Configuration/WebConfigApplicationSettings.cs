﻿using System.Configuration;
using System.Linq;
using System.Text;

namespace Match.WebUI.Infrastructure.Configuration
{
    public class WebConfigApplicationSettings : IApplicationSettings
    {
      

        public string JanrainApiKey
        {
            get
            {
                return ConfigurationManager
                             .AppSettings["JanrainApiKey"];
            }
        }

      /*  public string PayPalBusinessEmail
        {
            get { return ConfigurationManager.AppSettings["PayPalBusinessEmail"]; }
        }

        public string PayPalPaymentPostToUrl
        {
            get { return ConfigurationManager.AppSettings["PayPalPaymentPostToUrl"]; }
        }

        */
    }

}
