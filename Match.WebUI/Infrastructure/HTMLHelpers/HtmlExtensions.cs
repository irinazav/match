﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text;
using System.Web.Routing;
using Match.Domain.Entities;
using Match.Domain.Abstract;


namespace Match.WebUI.Infrastructure.HTMLHelpers
{

  
    public static class Helper1

    {

        public static MvcHtmlString CheckBoxes<T>(this HtmlHelper helper, IEnumerable<T> src, long mask,
              string name) where T : IAttribute
        {

            StringBuilder str = new StringBuilder(""); 
            
                  
                   foreach (var elem in src)
                   {
                       long currentmask = (long)(Math.Pow(2, elem.Id));
                       string val = elem.Id.ToString();

                       str.Append("<li><input id='" + name + val + "' name='Profile." +
                           name + "' type='checkbox' value='" + val + "'  ");

                     

                       if ((currentmask & mask)  > 0)
                       {    
                          str.Append(" checked='checked' />");
                           
                       }
                       else
                       
                       {       
                             str.Append(" />");
                       }

                       str.Append(" <label for='" + name + elem.Id.ToString() + "'>" + elem.Name + "</label></li>");

                   }

                   return new MvcHtmlString(str.ToString());
                   
          }

   }
    //----------------------------------------------------------------------------------
    public static class Helper2
    {

        public static MvcHtmlString AttributeText<T>(this HtmlHelper helper, IEnumerable<T> src) where T : IAttribute
        {


            string str = string.Join(", ", src.Select(p => p.Name).ToArray());
            return new MvcHtmlString(str);

        }

    }
    

    //----------------------------------------------------------
    public static class Helper3

    {
        public static MvcHtmlString Image(this HtmlHelper helper, string src, string altText, string height)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altText);
            builder.MergeAttribute("height", height);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
    }


    //------------------------------------------------------
    public static class HtmlExtensions
    {

        public static MvcHtmlString Button(this HtmlHelper helper,
                                     string innerHtml,
                                     object htmlAttributes)
        {
            return Button(helper, innerHtml,
                          HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes)
            );
        }

        public static MvcHtmlString Button(this HtmlHelper helper,
                                           string innerHtml,
                                           IDictionary<string, object> htmlAttributes)
        {
            var builder = new TagBuilder("button");
            builder.InnerHtml = innerHtml;
            builder.MergeAttributes(htmlAttributes);
            return MvcHtmlString.Create(builder.ToString());
        }

        public static MvcHtmlString ListBoxForIAttributeSource<T>(this HtmlHelper htmlHelper,
                                            string name, int selectedValue,
                                            IEnumerable<T> values,
                                            string zeroValue) where T : IAttribute
        {



            IEnumerable<SelectListItem> items =
               from value in values
               select new SelectListItem
               {
                   Text = value.Name.ToString(),
                   Value = value.Id.ToString(),
                   Selected = (value.Id == selectedValue)
               };

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.ListBox(
                name,
                items
                );
        }



        //---------------------------------------------------------
        public static MvcHtmlString DropDownListForIAttributeSource<T>(this HtmlHelper htmlHelper,
                                     string name, int selectedValue,
                                     IEnumerable<T> values,
                                     string zeroValue) where T : IAttribute
        {
            


            IEnumerable<SelectListItem> items =
               from value in values
               select new SelectListItem
               {
                   Text = value.Name.ToString(),
                   Value = value.Id.ToString(),
                   Selected = (value.Id == selectedValue)
               };

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownList(
                name,
                items
                );
        }

        //---------------------------------------------------------
        public static MvcHtmlString DropDownListForIAttributeSourceObject<T>(this HtmlHelper htmlHelper,
                                     string name, T selectedObject,
                                     IEnumerable<T> values, 
                                     string zeroValue) where T : IAttribute
        {
            int selectedValue = 0;
            if (selectedObject != null) selectedValue = selectedObject.Id;
           

            IEnumerable<SelectListItem> items =
               from value in values
               select new SelectListItem
               {
                   Text = value.Name.ToString(),
                   Value = value.Id.ToString(),
                   Selected = (value.Id == selectedValue)
               };
                          
            if (zeroValue != null)  {
                    IEnumerable<SelectListItem> zeroItems =
                                 from zero in values.Take(1)
                                 select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                    items = zeroItems.Union(items);
                }

            return htmlHelper.DropDownList(
                name,
                items
                );
        }
//------------------------------------------------------------------------------------------------------
        public static MvcHtmlString MatchEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,

                                Expression<Func<TModel, TEnum>> expression, string zeroValue)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                });


            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
        
        //---------------------------------------------------------
        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, 
                                string name, TEnum selectedValue, string zeroValue)
        {  
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                from value in values
                select new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = (value.Equals(selectedValue))
                };

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownList(
                name,
                items
                );
        }



        //-----------------------------------------------------------
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,
                                
                                Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
        
        
        //-----------------------------------------------------------
        public static MvcHtmlString EnumCheckBoxFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,
                                Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                   // Selected = value.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }

        //------------------------------------------------------------------
        //ProductAdmin
        //  @Html.ProductAttributeDropDownList(attr.Categories, "Category", Model.Category.Id) 


        public static MvcHtmlString ProductAttributeDropDownList(this HtmlHelper htmlHelper,
                                IEnumerable<IAttribute> list, 
                                string name, int selectedValue) 
        {
            

            IEnumerable<SelectListItem> items =
                from item in list
                select new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString(),
                    Selected = (item.Id.Equals(selectedValue))
                };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }

        //-----------------------------------------------------------------------------------------------------

        ///ProductAdmin
        //  @Html.ProductAttributeDropDownListFor(model => model.Id, attr.Categories)

        public static MvcHtmlString ProductAttributeDropDownListFor<TModel, T> (this HtmlHelper<TModel> htmlHelper,
                                        Expression<Func<TModel, T>> expression,
                                              IEnumerable<IAttribute> values)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.Name,
                    Value = value.Id.ToString(),
                    Selected = value.Id.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
      
    }
}